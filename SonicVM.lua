--[[>

    SonicVM -<>- XenusSoft

    This software is protected by the GNU General Public License v3.0.
    Do not distrubute without proper permission.

    <]]--

    local oldPullEvent = os.pullEvent
    os.pullEvent = os.pullEventRaw
    
    function drawLogo(x, y)
        w, h = term.getSize()
        local logo = {
            "S O N I C   ",
            "V M"
        }
        term.setCursorPos(x - (15/2), y)
        term.setTextColor(colors.blue)
        write(logo[1])
        term.setTextColor(colors.lightGray)
        write(logo[2])
    end
    
    w, h = term.getSize()
    
    function cwrite(txt, y)
        w, h = term.getSize()
        term.setCursorPos((w/2) - (#txt/2), y)
        term.clearLine()
        term.write(txt)
    end
    
    term.clear()
    drawLogo(w/2, h/2)
    sleep(1)

    function init() -- DO NOT REMOVE OR IT WILL BREAK!
    if true then
        error("This is not released, sorry.", 0)
    end
    
    
    
    
    
    
    end
    
    ok, err = pcall(init)
    
    
    
    if not ok then
        local choice = 0
        parallel.waitForAny(function()
        drawLogo((w/2) - (15/2), (h/2) + 2)
        term.setTextColor(colors.white)
        term.clear()
        cwrite("Uh oh, a complete error!", (h/2)-6)
        term.setTextColor(colors.red)
        cwrite(err, (h/2) - 1)
        term.setTextColor(colors.white)
        cwrite("Feel free to report this bug to fix.", (h/2) - 5)
        cwrite("We're updating to fix bugs...", (h/2) - 3)
        local dir = shell.getRunningProgram()
        fs.delete(dir)
        local f = io.open(dir, "w")
        local i = http.get("https://gitlab.com/xenussoft-computercraft-organization/other/sonicvm/-/raw/master/SonicVM.lua")
        local cont = i.readAll()
        f:write(cont)
        f:close()
        term.setCursorPos(1, (h/2) - 3)
        cwrite("Updated to latest version", (h/2) - 3)
        cwrite("Press enter to close", (h/2) - 1)
        cwrite("Press R to try again.", (h/2))
        local running = true
        while running do
            local ev, p1, p2, p3, p4, p5 = os.pullEventRaw()
            if ev == "key" then
                if p1 == keys.enter then
                    choice = 1
                    running = false
                end
                if p1 == keys.r then
                    choice = 2
                    running = false
                end
            end
        end
        if choice == 2 then
            shell.run(dir)
        end
        if choice == 1 then
            term.clear()
            term.setCursorPos(1,1)
            term.setTextColor(colors.yellow)
            term.setBackgroundColor(colors.black)
            print(os.version())
            term.setTextColor(colors.white)
            print("Thank you for using SonicVM!")
        end
        end, function() --<BREAK>--
        local msg = {
            "Copyright (c) 2020 - XenusSoft",
            "Do not distrubute without permission.",
            "For info, please visit us at",
            "https://igp-os-community.tribe.so",
            "You can also submit bugs on the CC Fourms,",
            "and the official website."
        }
        while true do 
        for i = 1, #msg do
        local oldX, oldY = term.getCursorPos()
        local oldBG = term.getBackgroundColor()
        local oldTXT = term.getTextColor()
        term.setTextColor(colors.gray)
        term.setBackgroundColor(colors.black)
        term.setCursorPos(1, h)
        term.clearLine()
        cwrite(msg[i], h)
        term.setTextColor(oldTXT)
        term.setBackgroundColor(oldBG)
        term.setCursorPos(oldX, oldY)
        sleep(5)
        end
        end
        end)
        if choice == 2 then
          shell.run(dir)
      end
      if choice == 1 then
          term.clear()
          term.setCursorPos(1,1)
          term.setTextColor(colors.yellow)
          term.setBackgroundColor(colors.black)
          print(os.version())
          term.setTextColor(colors.white)
          print("Thank you for using SonicVM!")
      end
      end
    
